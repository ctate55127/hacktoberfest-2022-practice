# Hacktoberfest 2022 Practice

UNWSP CI/CD class preparation for Hacktoberfest. Steps for participation:

1. Fork repository
2. Clone repository locally @ `git clone https://gitlab.com/[User]/hacktoberfest-2022-practice`
3. Create local branch with `git branch my_branch_name`
4. Checkout local branch with `git checkout my_branch_name` (-b flag to do both)
5. Add changes, `git add .` -> `git commit -m <message>`
6. Push to fork with `git push`
7. Pull request from fork back to upstream repository
8. Switch back to `master` branch locally with `git checkout master`
9. Pull upstream changes with `Ctrl+T` in IDE if needed

### Include your name or GitLab `@tag` below!